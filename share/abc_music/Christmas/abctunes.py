from entity.music import Abc_tune
Abc_tune.purge()

Silent_Night_28_X0 = Abc_tune(
    location='Silent_Night_28_X0',
    titles=('Silent Night, Holy Night',),
    filename='Silent_Night_28.abc',
    xref=0,
    composers=(),
    sources=()
)

O_Little_Town_F_Green_24_X0 = Abc_tune(
    location='O_Little_Town_F-Green_24_X0',
    titles=('O Little Town Of Bethlehem',),
    filename='O_Little_Town_F-Green_24.abc',
    xref=0,
    composers=(),
    sources=()
)

Hark_The_Herald_17_X0 = Abc_tune(
    location='Hark_The_Herald_17_X0',
    titles=('Hark! The Herald Angels Sing',),
    filename='Hark_The_Herald_17.abc',
    xref=0,
    composers=(),
    sources=()
)

In_The_Bleak_Midwinter_X1 = Abc_tune(
    location='In_The_Bleak_Midwinter_X1',
    titles=('In the Bleak Midwinter',),
    filename='In_The_Bleak_Midwinter.abc',
    xref=1,
    composers=('Gustav Holst',),
    sources=()
)

