#!/usr/bin/python3
# -*- encoding: utf8 -*-
from .abcIndexPage import AbcIndexPage, h
#from phileas.cherryweb.babelPage import BabelPage, h
from .abcTunesHelper import AbcTunesHelper
from phileas.cherryweb import url_and_qs
import cherrypy
from . import ABC_BASE, ABC_TMP


class AbcTunePage(AbcIndexPage):
# class AbcTunePage(BabelPage):
    mountPoint = '/abc_music'  # ???
    base_path = ABC_BASE

    _title = "ABC tunes"
    subdir = ''

    lexicon = {
        'ABC source': {'nl_NL':"ABC broncode"},
        'Score': {'nl_NL': "bladmuziek"},
    }

    def admin(self):
        return False

    def transpositionHack(self, *paths, **kw):
        yield from h.h2 % "Tranposition Options"
        yield from "Special 'one-click transpositions:" + h.br
        for sInstrument, transpose_by in (
                ('None',           0),
                ('(1 octave down) Cello',  -12),
                ('(2 octaves down) Cello', -24),
                ('(e.g. Clarinet in) Bb',   2),
                ('(e.g. Alt Sax in) Eb',    -3),
                ('(e.g. Horn in) F',        -5),
                ('(e.g. Euphonium in) Bb, 1 octave down', -10),
        ):
            yield from  h%('&nbsp;'*4) + h.a(href=url_and_qs(*paths,
                                                             gain=['set_session', 'abc_transpose_by', f"{transpose_by}"])) % sInstrument
            if transpose_by == int(cherrypy.session.get('abc_transpose_by', 0)):
                yield from h % " [=selected]"
        yield from h.br*2 + "general transposition:" + h.br
        action = url_and_qs(*paths, gain=['set_session', 'abc_transpose_by', '_semitones'],)
        print(f"AbcTunePage.transpositionHack: action={action}")

        def yield_field():
            yield from (h.label(For='semitones') % f'+ve or -ve number of semitones' +
                        f' <input type = "number" id="semitones" name="semitones" min="-48" max="48"' +
                        f'value="{cherrypy.session.get("abc_transpose_by", 0)}"><br />\n'
                        )

        yield from h.form(action=action, method='get') % yield_field()
        yield from h.br

    def upperBanner(self, *paths, **kw):
        return h.h1(id='upperbanner') % self(en_GB="ABC(plus) tune page", nl_NL="ABC(plus) tune page")

    def upperText(self, *paths, **kw):
        yield from AbcIndexPage.upperText(self, *paths, **kw)
        yield from self.transpositionHack(*paths, **kw)

    @cherrypy.expose
    def list(self, *paths, **kw):
        yield from self.wrapBody(self.BodyOfList, *paths, **kw)

    def BodyOfList(self, *paths, **kw):
        with AbcTunesHelper(self, self.subdir, *paths, **kw) as context_abc:
            yield from self.upperBanner(*paths, **kw)
            yield from self.upperText(*paths, **kw)
            yield from self.detail(*paths, **kw)
            yield from h.h2(id="lowerbanner") % (
                f"ABC Tune Collection:  {context_abc.paths_ and context_abc.paths_[-1] or '(uncategorized)'}")
            yield from context_abc.list_(**kw)

    def synopsis(self,*paths, **kw):
        return h.em % "[synopsis to be added]"

    def detail(self,*paths, **kw):
        return h.em % "[detail to be added]"

    @cherrypy.expose
    def view(self, *paths, **kw):
        yield from self.wrapBody(self.BodyOfView, *paths, **kw)

    def BodyOfView(self, tune_identifier, **kw):
        with AbcTunesHelper(self, self.subdir, **kw) as context_:
            yield from self.upperBanner(self.subdir, tune_identifier, **kw)
            yield from self.upperText(self.subdir, tune_identifier, **kw)
            yield from h.h2(id="lowerbanner") % self(en_GB=f"Viewing score and abc source of '{tune_identifier}'",
                                                     nl_NL=f"Bladmuziek en abc broncode van '{tune_identifier}' zijn hieronder getoond", )
            back = url_and_qs( lose=2, gain=['list'], qs={},
                                        fragment=f"#{tune_identifier}")
            # print(f"back={back}")  # also clear query string?
            yield from (h.a(id="back", href=back)) % "back to index" + h.br
            yield from context_.view_(tune_identifier, **kw)

_abcTunePage = AbcTunePage()
