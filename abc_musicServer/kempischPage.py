#!/usr/bin/python3
from .abcTunePage import AbcTunePage, h
class KempischPage(AbcTunePage):
    _title = "Kempisch VO ABC"

    subdir = "Kempisch"

    def synopsis(self, *p, **kw):
        yield from h % (self(
                                 en_GB="Repertoire of the ",
                                 nl_NL="Repertoire van het ",
                                 ) + "'Kempisch Volsorkest'"
                           )

    def detail(self, *p, **kw):
        yield from h.p % (
                self(
                      en_GB="N.B. This sheet music is also available in a variety of formats"
                            " on the official ",
                      nl_NL="Let op: Deze bladmuziek is ook te vinden in verschillende formaten op de officiële "
                      )
                + h.a(href="https://kempmuda.jimdofree.com/t-winkeltje/partituren-voor-diverse-instrumenten/")
                % 'site'
                + self(
                        en_GB=" of the orchestra (and dancers!) - except for a handful of tunes at then end of the list below;"
                              " these are experimental ABCplus renditions of some of the repertoire.",
                        nl_NL=" van het orkest (and dancers!) - behalve een handjevol liedjes onderaan het lijst hieronder; "
                              " dit zijn experimentele ABCplus bewerkingen van een kleine greep uit het repertoire.",

                        )
        )


_kempischPage = KempischPage()