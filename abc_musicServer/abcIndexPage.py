#!/usr/bin/python3
# -*- encoding: utf8 -*-
from phileas.cherryweb.babelPage import BabelPage, h
from . import ABC_BASE


class AbcIndexPage(BabelPage):
    _title = "ABC(plus) music"
    mountPoint = '/abc_music'  # ???
    # styleSheet =  os.path.join(ABC_BASE, "abc_music.css") # NO! DOESN'T WORK
    styleSheet =  "/abc_music/css/abc_music.css"
    base_path = ABC_BASE

    def upperText(self, *paths, **kw):
        yield from self.languageBlurb(*paths)
        yield from h.p % self(
            en_GB=(
                "I am a keen exponent of the ABC(plus) method of music notation."
                " This section of my website tries to showcaces some of its possibilities. "
            ),
            nl_NL=(
                "Ik ben een ijverige gebruiker van de ABC(plus) methode van muzieknotatie."
                " Dit gebied bnnen mijn website toont een aantal van diens mogelijkheden."
            ),
        )

    def upperBanner(self, *paths, **kw):
        return h.h1(id='upperbanner') % self(en_GB="My ABC(plus) music pages", nl_NL="ABC(plus) music pages")

    def lowerBanner(self, context_, **kw):
        return h.h1(id='lowerbanner') % self(en_GB="Information page",
                                              nl_NL="Informatie pagina")

    def lowerText(self, context_, **kw):
        yield from (
                h.p | (
            h.h3 | "Lots of information and software 'out there'",
            """
The ABC music format has been around for many many years. Over those years,
a lot of resources have become available. One place to find information about
these resources is
            """ +
            (h.a(href='http://abc.sourceforge.net/resources.html') |
             "The ABC Music project"),
            """
on 'sourceforge'.
            """,
            h.h3 | "My own notes on ABC plus",
            """
Perhaps following the above link (and the links within it) has made you
enthusiastic and sufficiently knowedgeable about ABC music to start installing
the necessary programs and start producing your own music with it.
            """
            + h.br*2
            + """
If, however, it has left you feeling lost and/or dizzy, the notes below
may help.
            """
            + h.br,
            h.h4 % "Quick start guide",
            h.p % (
                """
'EasyABC' is - as the name suggests - an easy program to install and use.
To install this, go to the
                """
                + (h.a(href=
                     'http://sourceforge.net/projects/easyabc/files/EasyABC/')
                 % "EasyABC site"
                 ),
                """
and download the latet version. At the time of writing, this direct
                """,
                h.a(href=
                    'http://sourceforge.net/projects/easyabc/files/EasyABC/1.3.6.4/'
                    'WindowsSetupEasy1364.exe/download') | 'link',
                """
will download the right installer for Windows users.
                """
                + h.h3 % "Music collections"
                + h.ul %
                ("""
                Click the links below to view the various music collections together with
                notes about their origins:
                                        """ + h.br*2,
                  [
                     h.li % (h.a(href=f'/abc_music/tune/{_subPage.subdir}/list') % _subPage.synopsis())
                            for _subPage in self.subPages
                 ]
                 )
            )
        )
        )

_abcIndexPage = AbcIndexPage()
