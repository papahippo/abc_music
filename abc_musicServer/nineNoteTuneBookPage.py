#!/usr/bin/python3
from .abcTunePage import AbcTunePage, h
class NineNoteTuneBookPage(AbcTunePage):
    _title = "9-note Tunebook (ABC)"

    subdir = "NineNoteTuneBook"

    def synopsis(self, *p, **kw):
        yield from h % (self(
            en_GB="The ",
            nl_NL="De ",
                                ) + "'Nine-note tune collection'"
                           )


_nineNoteTuneBookPage = NineNoteTuneBookPage()