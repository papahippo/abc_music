import cherrypy
import os, tempfile
HERE, _ = os.path.split(__file__)
ABC_BASE = os.path.normpath(os.path.join(HERE, '..', 'share', 'abc_music'))
ABC_TMP = os.path.join(tempfile.gettempdir(), 'abc_music')
print(f"ABC_BASE={ABC_BASE}, ABC_TMP={ABC_TMP}")

from .abcTunePage import _abcTunePage

from .christmasPage import _christmasPage
from .kempischPage import _kempischPage
from .nineNoteTuneBookPage import _nineNoteTuneBookPage
from .nottinghamPage import _nottinghamPage
from .samplesPage import _samplesPage
from .sinterKlaasPage import _sinterKlaasPage
from .theCelticRoomPage import _theCelticRoomPage

_abcSubPages = (_christmasPage, _kempischPage, _nineNoteTuneBookPage, _nottinghamPage,
                _samplesPage, _sinterKlaasPage, _theCelticRoomPage)
for _subPage in _abcSubPages:
    _abcTunePage.__setattr__(_subPage.subdir, _subPage)

from .abcIndexPage import _abcIndexPage
_abcIndexPage.subPages = _abcSubPages
_abcIndexPage.tune = _abcTunePage

from phileas.cherryweb import sites_served

def _abcConfig(session_path=''):
    return {
    '/':
        {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': '/tmp',  # HERE,
            'tools.sessions.on': True,
            'tools.sessions.storage_class': cherrypy.lib.sessions.FileSession,
            'tools.sessions.storage_path': session_path,
            'tools.sessions.timeout': 10,
        },
    '/css':
        {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': ABC_BASE + "/.css",
        },
    '/tmp':
        {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': '/tmp/abc_music',
        },
    }
sites_served .append(('/abc_music', _abcIndexPage, _abcConfig))

