from entity.music import Abc_tune
from phileas.cherryweb.entityHelper import *
from phileas.cherryweb import url_and_qs
from furl import furl
import sys, os, tempfile, subprocess
from . import ABC_TMP

# to be move to a 'util' directory soon?
class Extra:
    def __init__(self, myIterator, extra=None, *more_extras):
        self.myIterator = myIterator
        self.extra_values = list((extra,)+more_extras)

    def __iter__(self):
        return self

    def __next__(self):
        try:
            return next(self.myIterator)
        except StopIteration:
            try:
                return self.extra_values.pop(0)
            except IndexError:
                raise StopIteration


class AbcTunesHelper(EntityHelper):
    EntityClass = Abc_tune
    _moduleForImport = 'abctunes'

    sDefaultSortKey = 'titles'

    fieldDisplay = OrderedDict([
# python name       # heading for glossing                              # field entry tip for glossing
#('location',         ({'en_GB':'location',           'nl_NL':'locatie'},         {'en_GB':'location',           'nl_NL':'locatie'})),
('filename',         ({'en_GB':'file',           'nl_NL':'bestand'},         {'en_GB':'file',           'nl_NL':'bestand'})),
('xref',            ({'en_GB':'X (index)',       'nl_NL':'X (index)'},       {'en_GB':'X (index)',       'nl_NL':'X (index)'})),
('titles',          ({'en_GB':'title(s)',        'nl_NL':'titel(s)'},           {'en_GB':'title(s)',        'nl_NL':'titel(s)'})),
('composers', ({'en_GB': 'composer(s)', 'nl_NL': 'componist(en)'}, {'en_GB': 'composer(s)', 'nl_NL': 'componist(en)'})),
('sources',          ({'en_GB':'source(s)',        'nl_NL':'bron(nen)'},        {'en_GB':'source(s)',        'nl_NL':'bron(nen)'})),
    ])

    def yield_link_fields(self, entity,):
        if 1:
            view = url_and_qs( lose=1, gain=['view', getattr(entity, entity.keyFields[0])], qs={})
        else:
            furlo = furl(cherrypy.url())
            print(f"furlo={furlo}")
            view = (furlo.remove(path=('list', *self.paths_))
                    .add(path=('view', getattr(entity, entity.keyFields[0])))
                    .set(fragment='back'))
        print(f"view={view}")
        yield from (h.a(id=f'{getattr(entity, entity.keyFields[0])}',
                         href=str(view))
                     % self.page("view"))
        yield from h.br

    def include_entity(self, member):
        # This is a first attempt to make rows_per_entity kind of generic!
        #
        member.current = True
        return True   # stub/default?


    def view_(self, key_, exception_=None, filter_='', **kw):
        print(f'!!view_tune_  filter_={filter_}')
        abc_tune = Abc_tune.by_key(key_)
        print(abc_tune)
        print(self.import_path)
        full_input_filename = os.path.join(self.import_path, abc_tune.filename)
        #output_path = tempfile.mkdtemp(prefix='abc_work_')
        output_path = ABC_TMP
        os.makedirs(output_path, exist_ok=True)
        svg_url_name = os.path.join('/abc_music/tmp', f'{key_}001.svg')
        yield from h.img(src=svg_url_name, width='100%')

        single_abc_filename =  os.path.join(output_path, f'{key_}.abc')
        transpose_by = cherrypy.session.get('abc_transpose_by', 0)
        transpose_arg = f' -t {transpose_by}' if transpose_by else ''
        command = (
             f'abc2abc {full_input_filename}'
             f"{transpose_arg}"
             f' -e -xref {abc_tune.xref} '
             f' >{single_abc_filename}'
             )
        print(command)
        os.system(command)
        command = (
             f'abcm2ps {single_abc_filename}'
             f' -e {abc_tune.xref}'
             f' -g -O {os.path.join(output_path, key_)}'
             )
        print(command)
        os.system(command)
        with open(single_abc_filename) as source:
            yield from (h.pre | source.read())

    def buildModuleForImport(self):
        # correct the (widely advertised so must be supported!) form without path (usually 'current')
        print(f"'abctunes' index module not (yet) present: let's build it! ...")
        ## not generator thanks! yield from h.em % f"building tune index (import_path={self.import_path}) ..."
        # ... much to be added!
        # WARNING! following line is a creative stub!
        ## abc_tune = Abc_tune(filename='test.abc', title='imaginary tune', composer=['nonone'], arranger=['composer'])
        for fn in os.listdir(self.import_path):
            base_, ext_ = os.path.splitext(fn)
            if ext_.lower() != '.abc':
                continue
            print(base_)
            xref = None
            titles = []
            composers = []
            sources = []
            for l in Extra(open(os.path.join(self.import_path, fn), 'r'), 'X:'):
                rest_of_line = l[2:].strip()
                if l.startswith('X:'):
                    if xref is not None:
                        # print("inter", xref)
                        Abc_tune(location=f'{base_}_X{xref}',
                                 titles=titles,
                                 filename=fn,
                                 xref=xref,
                                 composers=composers,
                                 sources=sources,
                                 )
                    if not rest_of_line:  # => artificial extra 'X:' record.
                        break
                    xref = int(rest_of_line)
                    titles = []
                    composers = []
                    sources = []
                elif l.startswith('S:'):
                    sources.append(rest_of_line)
                elif l.startswith('T:'):
                    titles.append(rest_of_line)
                elif l.startswith('C:'):
                    composers.append(rest_of_line)

        Abc_tune.export(f'{self.import_path}/abctunes.py')
        Abc_tune.purge()
        return True  # = 'yes I have rebuilt the module'
