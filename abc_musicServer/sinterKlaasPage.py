#!/usr/bin/python3
from .abcTunePage import AbcTunePage, h
class SinterKlaasPage(AbcTunePage):
    _title = "ABC tune samples"

    subdir = "samples"

    def synopsis(self, *p, **kw):
        yield from h % self(
            en_GB="Songs for 'Sinter Klass'",
            nl_NL="Sinter Klaas liedjes "
                                )

    def detail(self, *p, **kw):
        yield from h.p % (
        self(
              en_GB="N.B. This sheet music is also available in a variety of formats"
                    " on this ",
              nl_NL="Let op: Deze bladmuziek is ook te vinden in verschillende formaten op deze "
              )
        + h.a(href="https://musicad.nl/sinterklaas-lied")
            % 'site'
        + self(
                en_GB=" together with the accompanying lyrics.",
                nl_NL=" samen met de bijbehorende zangteksten."
                )
        )
        yield from h.p % (
            self(
                  en_GB="I have 'untrasposed' (at least) one voice in one tune"
                        " to fit in better with my own site's transposition options",
                  nl_NL="Ik heb (minstens) één stem van één liedje 'onttransponeerd om"
                         " beter te passen met de transpositie mogelijkheden van mijn site."
                  )
    )

_sinterKlaasPage = SinterKlaasPage()