#!/usr/bin/python3
from .abcTunePage import AbcTunePage, h
class ChristmasPage(AbcTunePage):
    _title = "Christmas tunes (ABC)"

    subdir = "Christmas"

    def synopsis(self, *p, **kw):
        yield from h % self(
                             en_GB="Chrsitmas hymns and carols",
                             nl_NL="Kerstzang"
                             )


    def detail(self, *p, **kw):
        yield from h.p % (
            self(
                  en_GB="This music was hastily copied from various sources on Christmas Eve, 2020 ",
                  nl_NL="Deze muziek is met haast samengesteld, Kerstavond 2020."
                  )
        ) + h.br


_christmasPage = ChristmasPage()