#!/usr/bin/python3
from .abcTunePage import AbcTunePage, h
class TheCelticRoomPage(AbcTunePage):
    _title = "The Celtic Room ABC tunes"

    subdir = "TheCelticRoom"

    def synopsis(self, *p, **kw):
        yield from h % (self(
            en_GB="Repertoire of ",
            nl_NL="Repertoire van ",
        ) + "'The Celtic Room'"
                           )

    def detail(self, *p, **kw):
        yield from h.p % (self(
            en_GB="N.B. This sheet music is also available in a variety of formats"
                  " on the official ",
            nl_NL="Let op: Deze bladmuziek is ook te vinden in verschillende formaten op de officiële "
        )
                          + h.a(href="https://thecelticroom.org/")
                          % 'site'
                          + self(
                                  en_GB=" of 'The Celtic Room' - housed in Indonesia but definitely Irish in spirit!",
                                  nl_NL=" van 'The Celtic Room' - gehuisvest in Indonesië maar beslist Iers in geest!",
                                  )
                          )


_theCelticRoomPage = TheCelticRoomPage()