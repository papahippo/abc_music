#!/usr/bin/python3
from .abcTunePage import AbcTunePage, h
class SamplesPage(AbcTunePage):
    _title = "ABC tune samples"

    subdir = "samples"

    def synopsis(self, *p, **kw):
        yield from h % self(
            en_GB="Miscellaneous tunes in ABC(plus) format",
            nl_NL="Diverse liedjes in ABC(plus) formaat",
                           )


    def detail(self, *p, **kw):
        yield from h.p % (self(
                                en_GB="Most of these tunes are taken from the source distribution of ",
                                nl_NL="Het merendeel van deze liedjes wordt meegleverd met de broncode van ",
                                )
                          + h.a(href='https://github.com/leesavide/abcm2ps') % "abcm2ps" +
                          self(
                                en_GB=" - a tool for generating the sheet music from ABC source code.",
                                nl_NL=" - een programma om blad muziek te genereren uit ABC broncode.",
                                )
                          ) + h.p % self(
                                          en_GB="This is also a convenient place for me to put other miscellaneous ABC source material "
                                                " that doesn't belong to a specific collection.",
                                          nl_NL="Dit is ook een handig plekje om andere ABC materie te bewaren dat"
                                                " niet hoort bij welke collectie dan ook.",
                                          )

_samplesPage = SamplesPage()