#!/usr/bin/python3
from .abcTunePage import AbcTunePage, h
class NottinghamPage(AbcTunePage):
    _title = "Nottingham ABC tunes"

    subdir = "Nottingham"

    def synopsis(self, *p, **kw):
        yield from h % (self(
            en_GB="The ",
            nl_NL="De ",
                                ) + "'Nottingham Collection of traditional tunes'"
                           )


    def detail(self, *p, **kw):
        yield from h.p % (self(en_GB="N.B. This sheet music is also available on the official ",
              nl_NL="Let op: Deze bladmuziek is ook te vinden op de officiële "
              )
        + h.a(href="http://www.cs.nott.ac.uk/Department/Staff/ef/database.html")
            % 'Nottingham Music Database site'
        )

_nottinghamPage = NottinghamPage()