"""
MusicRaft
"""

import sys, os, pathlib
from setuptools import setup, find_packages

# The directory containing this file
HERE = pathlib.Path(__file__).parent

# The text of the README file
README = (HERE / "README.md").read_text()

SHARE = pathlib.Path('share/abc_music')
share_dirs = [str(SHARE / Dir) for Dir in os.listdir(SHARE)]

share_dirs_here = [str(HERE / share_dir) for share_dir in share_dirs]

data_files = [(share_dir, [os.path.join(share_dir, each_file) for each_file in os.listdir(share_dir_here) if not each_file.startswith('__')])
            for share_dir, share_dir_here in zip(share_dirs, share_dirs_here)]


print(data_files)
# This call to setup() does all the work
setup(name = 'abc_music',
    version = '0.1.6',
    description='ABC(plus) music web server',
    long_description=README,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/papahippo/abc_music',
    author = "Larry Myerscough",
    author_email='hippostech@gmail.com',
    packages=['abc_musicServer',],
    scripts=['launch_server.py',],
    data_files=data_files,
    license='LICENSE.txt',
    install_requires=[ #commented out entries relate to packages needed by out-of-service plugin 'freqraft'.
        "phileas",
    ],
)
